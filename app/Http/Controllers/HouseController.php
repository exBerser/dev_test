<?php

namespace App\Http\Controllers;

use App\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    /**
     * View base blade
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Get data by filter
     *
     * @param Request $request
     * @return mixed
     */
    public function fetchAll(Request $request)
    {
        return House::filterData($this->getAllParams($request));
    }

    /**
     * Fetch parameters from get methods
     *
     * @param $request
     * @return array
     */
    private function getAllParams($request)
    {
        $filters = [
            'Name' => [
                'operators' => 'LIKE',
                'value' => ''
            ],
            'Price' => [
                'operators' => 'BETWEEN',
                'value' => ''
            ],
            'Bedrooms' => [
                'operators' => 'EQUAL',
                'value' => ''
            ],
            'Bathrooms' => [
                'operators' => 'EQUAL',
                'value' => ''
            ],
            'Storeys' => [
                'operators' => 'EQUAL',
                'value' => ''
            ],
            'Garages' => [
                'operators' => 'EQUAL',
                'value' => ''
            ]
        ];

        foreach ($filters as $fieldName => $filter) {
            if ($request->has($fieldName)) {
                $filters[$fieldName]['value'] = $request->get($fieldName);
            }
        }

        return $filters;
    }
}
