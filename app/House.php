<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    public $fillable = ['Name', 'Price', 'Bedrooms', 'Bathrooms', 'Storeys', 'Garages'];


    /**
     * Generate filter and get data
     *
     * @param array $filters
     * @return mixed
     */
    public static function filterData(array $filters)
    {
        $query = null;
        $where = [];

        foreach ($filters as $fileName => $field) {
            switch ($field['operators']) {
                case 'LIKE':
                    $where[] = [$fileName, 'like', '%' . $filters[$fileName]['value'] . '%'];

                    break;
                case 'BETWEEN':
                    $between = json_decode($field['value']);

                    if ( empty($between->min) && empty($between->max) ) {
                        continue;
                    }

                    $between->min = $between->min === '' ? 0 : $between->min;
                    $between->max = empty($between->max) ? PHP_INT_MAX : $between->max;

                    if ($query === null) {
                        $query = House::whereBetween($fileName, [$between->min, $between->max]);
                    } else {
                        $query->whereBetween($fileName, [$between->min, $between->max]);
                    }
                    break;
                case 'EQUAL':
                    if ( empty($field['value']) ) {
                        continue;
                    }

                    $where[] = [$fileName, '='  , $filters[$fileName]['value']];

                    break;
            }
        }

        if ($query === null) {
            $query = House::where($where);
        } else {
            $query->where($where);
        }

        return $query->get();
    }
}