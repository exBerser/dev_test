<?php

use App\House;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class HouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path() . '/csv/property-data.csv';

        $data = Excel::load($path, function($reader) {})->get()->toArray();

        array_walk($data, function (&$record) {
            $record['created_at'] = \Carbon\Carbon::now()->toDateTimeString();
            $record['updated_at'] = \Carbon\Carbon::now()->toDateTimeString();
        });

        House::insert(
            $data
        );
    }
}
